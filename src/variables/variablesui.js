import Plugin from '@ckeditor/ckeditor5-core/src/plugin';

import { addListToDropdown, createDropdown } from '@ckeditor/ckeditor5-ui/src/dropdown/utils';

import Collection from '@ckeditor/ckeditor5-utils/src/collection';
import Model from '@ckeditor/ckeditor5-ui/src/model';

export default class VariablesUI extends Plugin {
    init() {
        const editor = this.editor;
        const t = editor.t;

        // Получаем переданную настройку
        const variables = editor.config.get( 'variablesConfig.data' );

        // The "placeholder" dropdown must be registered among the UI components of the editor
        // to be displayed in the toolbar.
        editor.ui.componentFactory.add( 'variable', locale => {
            const dropdownView = createDropdown( locale );

            // Populate the list in the dropdown with items.
            if (Array.isArray(variables) && variables.length > 0) {
                const dropdownItemsDefinitions = getDropdownItemsDefinitions(variables);
                addListToDropdown( dropdownView, dropdownItemsDefinitions );
            }

            dropdownView.buttonView.set( {
                // The t() function helps localize the editor. All strings enclosed in t() can be
                // translated and change when the language of the editor changes.
                label: t( 'Вставить переменную' ),
                tooltip: true,
                withText: true
            } );

            // Disable the placeholder button when the command is disabled.
            const command = editor.commands.get( 'variable' );
            dropdownView.bind( 'isEnabled' ).to( command, () => {
                return Array.isArray(variables) && variables.length > 0;
            } );

            // Execute the command when the dropdown item is clicked (executed).
            this.listenTo( dropdownView, 'execute', evt => {
                editor.execute( 'variable', { value: evt.source.commandParam } );
                editor.editing.view.focus();
            } );

            return dropdownView;
        } );
    }
}

function getDropdownItemsDefinitions( variables = [] ) {
    const itemDefinitions = new Collection();

    for ( const variable of variables ) {
        const definition = {
            type: 'button',
            model: new Model( {
                commandParam: variable.code,
                label: variable.title,
                withText: true
            } )
        };

        // Add the item definition to the collection.
        itemDefinitions.add( definition );
    }

    return itemDefinitions;
}
