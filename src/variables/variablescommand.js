// placeholder/placeholdercommand.js

import Command from '@ckeditor/ckeditor5-core/src/command';

export default class VariablesCommand extends Command {
    execute( { value } ) {
        const editor = this.editor;
        const selection = editor.model.document.selection;

        editor.model.change( writer => {
            // Create a <variable> element with the "name" attribute (and all the selection attributes)...
            const variable = writer.createElement( 'variable', {
                ...Object.fromEntries( selection.getAttributes() ),
                name: value
            } );

            // ... and insert it into the document.
            editor.model.insertContent( variable );

            // Put the selection on the inserted element.
            writer.setSelection( variable, 'on' );
        } );
    }

    refresh() {
        const model = this.editor.model;
        const selection = model.document.selection;

        const isAllowed = model.schema.checkChild( selection.focus.parent, 'variable' );

        this.isEnabled = isAllowed;
    }
}
